# Decentralized Wikipedia

Welcome to the DAAR project. The idea of this project was implementing a complete Wikipedia
in a decentralized way, on Ethereum plateform. This will have cool side effects, like not
be forced to pay for servers.

*Implemented By:*
- Shokoufeh Ahmadi Simab
- Farhad Taheri


Requirement:
[`Ganache`](https://www.trufflesuite.com/ganache) 
[`Node.js`](https://nodejs.org/en/)
[`NPM`](https://www.npmjs.com/)
[`Yarn`](https://yarnpkg.com/)
[`Metamask`](https://metamask.io/)


# Some setup

Once everything is installed, launch `Ganache`. Create a new workspace, give it a name, and accept. You should have a local blockchain running in local. Now you can copy the mnemonic phrase Ganache generated, open Metamask, and when it asks to import a mnemonic, paste the mnemonic. Create the password of your choice and that’s fine.
Now you can connect Metamask to the blockchain. To do this, add a network by clicking on `main network` and `personalized RPC`. Here, you should be able to add a network.
Once you have done it, you’re connected to the Ganache blockchain!


# Properties

The program is be able to:

- Read an article by its ID (an uint).
- Someone can be able to submit a new article with content.
- Someone can be able to update an article.
- Someone can be see all his update history.
