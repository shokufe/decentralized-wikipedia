pragma solidity ^0.5.0;

contract Wikipedia {
  struct Article {
    uint id;
    string content;
    address author;
  }

 struct History {
    uint id;
    uint idArticle;
    string newcontent;
    string oldcontent;
    address author;
    uint256  date;
  }
  // uint[] public ids;
  // changed articlesbyid to articles
  mapping (uint => Article) public articles;
 
  // keep track of the number of articles added
  uint public articleCount = 0;
   string public _oldContent = " " ;
  mapping (uint => History) public histories;
  uint public historyCount = 0; 

  // For unit test purposes
  event ArticleCreated(
    uint id,
    string content,
    address author
  );

  event ArticleUpdated(
    uint id,
    string content,
    address author
  );
  
  event UpdatedHistory(
    uint id,
    uint idArticle,
    string newcontent,
    string oldcontent,
    address author,
    uint256  date
  );


  constructor() public {
    createArticle("This is my first article");
  }

  function articleContent(uint index) public view returns (string memory) {
    return articles[index].content;
  }

  // function getAllIds() public view returns (uint[] memory) {
  //   return ids;
  // }

  // Write your code here.
  function createArticle(string memory _content) public {
    // Require valid content
    require(bytes(_content).length > 0);
    // Increment the article count
    articleCount ++;
    // Create the post
    articles[articleCount] = Article(articleCount, _content, msg.sender);
    // Trigger event for checking the data in unit test
    emit ArticleCreated(articleCount, _content, msg.sender);
  }

  function updateArticle(uint _id, string memory _content) public {
    // Fetch the article which going to be edit
    Article memory _article = articles[_id];
    // Fetch the author of the article
    address _author = _article.author;
    // Check the person be the same
    require(_author==msg.sender);
    // Change the content
     _oldContent=_article.content;
    _article.content = _content;
    // Update the article
    articles[_id] = _article;
    // Trigger event for checking the data in unit test
    historyofUpdate(_id, _article.content,_oldContent,_author);
    emit ArticleUpdated(articleCount, _article.content, _author);
  }
  /**
  * save update history with its old and new content
  *
   */
  function historyofUpdate(uint _id2, string memory _content, string memory _oldcontent, address _author2) public {
    require(bytes(_content).length > 0);
    require(bytes(_oldcontent).length > 0);
    historyCount++;
    histories[historyCount] = History(historyCount, _id2,_content,_oldcontent,_author2, now);
    emit UpdatedHistory(historyCount, _id2,_content,_oldcontent,_author2, now);
  }
}
