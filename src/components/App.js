import React, { Component } from 'react';
import Web3 from 'web3';
import './App.css';
import Wikipedia from '../build/contracts/Wikipedia.json'
import Navbar from './Navbar'

import Main from './Main'
import AllArticles from './allArticles'
import History from './History.js'
import ReactNotification from 'react-notifications-component'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      account: '',
      wikipedia: null,
      articleCount: 0,
      historyCount:0,
      articles: [],
      loading: true,
      cibleArticle: null,
      cibleAuthor:null,
      cibleArticleId:null,
      finded:false,
      createPage:true,
      showallpage:false,
      historypage:false,
      histories: []
    }
    this.createArticle = this.createArticle.bind(this)
    this.searchArticle = this.searchArticle.bind(this)
    this.handleCreate=this.handleCreate.bind(this)
    this.handleShowall=this.handleShowall.bind(this)
    this.handleHistory=this.handleHistory.bind(this)
    this.gethistories()
    this.gethistories=this.gethistories.bind(this)


  }
  async componentWillMount() {
    await this.loadWeb3()
    await this.loadBlockchainData()
  }

  async loadWeb3() {
    if (window.ethereum) {
      window.web3 = new Web3(window.ethereum)
      await window.ethereum.enable()
    }
    else if (window.web3) {
      window.web3 = new Web3(window.web3.currentProvider)
    }
    else {
      window.alert('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  }

  async loadBlockchainData() {
    const web3 = window.web3
    // Load account
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
    // Network ID
    const networkId = await web3.eth.net.getId()
    const networkData = Wikipedia.networks[networkId]
    
    if(networkData) {
      
      const wikipedia = new web3.eth.Contract(Wikipedia.abi, networkData.address)
      console.log("wikipedia22: ",wikipedia.methods.articleCount().call())
      console.log("wikipedia22: ",wikipedia.methods.historyCount().call())
      this.setState({ wikipedia })
     
      const articleCount = await wikipedia.methods.articleCount().call();
      const historyCount = await wikipedia.methods.historyCount().call();
      console.log(articleCount)
      this.setState({ articleCount })
      console.log(articleCount)
    
      console.log(historyCount)
      this.setState({ historyCount })
      console.log(historyCount)
      // Load Posts
      for (var i = 1; i <= articleCount; i++) {
        const article = await wikipedia.methods.articles(i).call()
        this.setState({
          // Add the new one at the end of pervious array
          articles: [...this.state.articles, article]
        })
      }
     
      for (var j = 1; j <= historyCount; j++) {
        const history1 = await wikipedia.methods.histories(j).call()
        this.setState({
          // Add the new one at the end of pervious array
          histories: [...this.state.histories, history1]
        })
         console.log({ articles: this.state.articles })
         this.setState({ loading: false})
      } 
    }
    else {
      window.alert('Wikipedia contract not deployed to detected network.')
    }
  }
  gethistories(){

 
  }
  /**
 * handling the navbar and routing between pages
 */
handleHistory(){
  this.setState({showallpage:false,createPage:false,historypage:true});
}
handleCreate(){
  this.setState({showallpage:false,createPage:true,historypage:false});
}
handleShowall(){
 this.setState({createPage:false,showallpage:true,historypage:false});
}
/**
 * Create an article and add it to DB of blockchain
 * @param {*} content 
 */
  createArticle(content) {
    this.setState({ loading: true })
    this.state.wikipedia.methods.createArticle(content).send({ from: this.state.account })
    .once('receipt', (receipt) => {
      this.setState({ loading: false })
    })
    window. location. reload(false);
  }
  /**
   * Search an article based on it's id
   * @param {*} _id 
   */
  searchArticle(SearchedId) {
    for(var i = 0; i < this.state.articleCount; i++) {
      if(this.state.articles[i].id === SearchedId){
        this.setState({ 
                        finded: true,
                        cibleArticle: this.state.articles[i].content,
                        cibleAuthor: this.state.articles[i].author,
                        cibleArticleId : this.state.articles[i].id
                      })
      }
    }
  }
  /**
   * routing between pages
   */
  render() {
    return (
      <div style={{backgroundColor:'#F5F5F5'}}>
        <ReactNotification />
        <Navbar account={this.state.account} />
        <div style={{marginTop:40,textAlign:"center",padding: 2,paddingLeft:22 ,backgroundColor:'#1E1E1E'}}>
            <button type="submit" className="btn" style={{backgroundColor:'#76AC00'}} onClick={this.handleShowall}>Show All</button>
            <button type="submit" className="btn" style={{backgroundColor:'#76AC00'}} onClick={this.handleCreate}>Add an article</button>
            <button type="submit" className="btn" style={{backgroundColor:'#76AC00'}} onClick={this.handleHistory}>My History</button>
        </div>
        
        { this.state.loading 
          ? <div id="loader" className="text-center mt-5"><p>Loading...</p></div>
          : <div></div>

        }
        {this.state.createPage
           ? <Main
                  articles={this.state.articles}
                  createArticle={this.createArticle}
                  searchArticle={this.searchArticle}
                  resultSearch={this.state.cibleArticle}
                  resultAuthor={this.state.cibleAuthor}
                  resultArticleId ={this.state.cibleArticleId}
                  wikipedia={this.state.wikipedia}
                  account={this.state.account}
                  finded={this.state.finded}
             />
           :<div></div>
        }
        { this.state.showallpage
            ?<AllArticles 
                 articles={this.state.articles}
                 wikipedia={this.state.wikipedia}
                 account={this.state.account}
            />
            :<div></div>
        }

       { this.state.historypage
            ?<History 
                 histories={this.state.histories}
                 historyCount={this.state.historyCount}
                 wikipedia={this.state.wikipedia}
                 account={this.state.account}
            />
            :<div></div>
        }
      
      </div>
    );
  }
}

export default App;