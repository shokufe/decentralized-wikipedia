import React, { Component } from 'react';
//import Identicon from 'identicon.js';
import 'react-notifications-component/dist/theme.css'
import { store } from 'react-notifications-component';


class Modify extends Component {

  constructor(props){
    super(props);
    this.state={modifyclick:false , recive:false} 
    this.updateContent=this.updateContent.bind(this)
  }
  /**
   * send upate content to blockchain (new refresh page to show result)
   * @param {*} inputContentChange : new content 
   */
  updateContent(inputContentChange){
    this.props.wikipedia.methods.updateArticle(this.props.resultid,inputContentChange).send({ from: this.props.account })
    .once('receipt', (receipt) => {
      this.setState({ recive: true })
    })

  }
  render() {
    return (
      <div className="container-fluid mt-5" style={{backgroundColor:'#F5F5F5'}}>
        <div className="row">
          <main role="main" className="col-lg-12 ml-auto mr-auto" style={{ maxWidth: '500px' }}>
            <div className="content mr-auto ml-auto">
              <form onSubmit={(event) => {
                  event.preventDefault()
                  const inputContentChange = this.articleNewContent.value
                  this.updateContent(inputContentChange)
                }}>
                <div className="form-group mr-sm-2">
                  <div>Author:{this.props.resultauthor}</div>
                  <div>ID of Article :{this.props.resultid}</div>
                  <textarea
                    id="articleNewContent"
                    type="text"
                    ref={(input) => { this.articleNewContent = input }}
                    className="form-control"
                    placeholder={this.props.resultarticle}
                    required />
                </div>
                <button type="submit" class="btn" style={{backgroundColor:'#76AC00'}}>Update</button>
              </form>

            </div>
          </main>
        </div>
      </div>
    );
  }
}

export default Modify;