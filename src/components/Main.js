import React, { Component } from 'react';
//import Identicon from 'identicon.js';
import 'react-notifications-component/dist/theme.css'
//import Modify from './Modify'
import { store } from 'react-notifications-component';

class Main extends Component {
  constructor(props){
    super(props);
    this.state={modifyclick:false, searchModifyActive:false, cibleArticle:null,cibleAuthor:null,cibleArticleId:null,finded:false}
    this.handleModify = this.handleModify.bind(this)
    this.handleSearch=this.handleSearch.bind(this)
    this.updateContent=this.updateContent.bind(this)
  }
  /**
   * updatecontent send request to blockchain
   * @param {} inputContentChange : new content
   */
  updateContent(inputContentChange){
    alert(this.props.resultArticleId)
    this.props.wikipedia.methods.updateArticle(this.props.resultArticleId,inputContentChange).send({ from: this.props.account })
    .once('receipt', (receipt) => {
      this.setState({ recive: true })
    })
  //  window. location. reload(false);
  }
  /**
   * search an article by its id
   * @param {*} SearchedId 
   */
  searchArticleMain(SearchedId) {
  
    for(var i = 0; i < this.props.articleCount; i++) {
      if(this.props.articles[i].id === SearchedId){
        this.setState({ 
                        finded: true,
                        cibleArticle: this.state.articles[i].content,
                        cibleAuthor: this.state.articles[i].author,
                        cibleArticleId : this.state.articles[i].id
                      })
      }
    }
  }
  /**
   * handle modify by click on modify button
   */
  handleModify(){
    this.setState(  {
      modifyclick:true }); 
  }
  /**
   * handle search by click on search button
   */
  handleSearch(){
    if(this.props.finded===true){
      this.setState({searchModifyActive:true});
    }
  }
  render() {
    return (
      <div className="container-fluid mt-5" style={{backgroundColor:'#F5F5F5'}}>
        <div className="row">
          <main role="main" className="col-lg-12 ml-auto mr-auto" style={{ maxWidth: '500px' }}>
            <div className="content mr-auto ml-auto">
              <p>&nbsp;</p>
                <form onSubmit={(event) => {
                  event.preventDefault()
                  const content = this.articleContent.value
                  this.props.createArticle(content)
                }}>
                <div className="form-group mr-sm-2">
                  <textarea
                    id="articleContent"
                    type="text"
                    ref={(input) => { this.articleContent = input }}
                    className="form-control"
                    placeholder="What's on your mind?"
                    required />
                </div>
                <button type="submit" class="btn" style={{backgroundColor:'#76AC00'}}>Add Article</button>
              </form> 
              <p>&nbsp;</p>
              <div>
              <form onSubmit={(event) => {
                  event.preventDefault()
                  const inputSearch = this.articleID.value
                  this.props.searchArticle(inputSearch)
                }}>
                <div className="form-group mr-sm-2">
                  <input
                    id="articleID"
                    type="text"
                    ref={(input) => { this.articleID = input }}
                    className="form-control"
                    placeholder="Enter the article id"
                    required />
                </div>
                <button type="submit" class="btn"  style={{backgroundColor:'#76AC00'}} onClick={this.handleSearch}>Search</button>
              </form>
              </div>
              <p>&nbsp;</p>
              {this.state.searchModifyActive
                ? <div className="card mb-4">
                    <div className="card-header">
                      <small className="text-muted">{this.props.resultAuthor}</small>
                    </div>
                    <ul id="articleList" className="list-group list-group-flush">
                      <li className="list-group-item">
                        <p>{this.props.resultSearch}</p>
                        <button type="submit"  class="btn" style={{backgroundColor:'#76AC00'}} onClick={this.handleModify}>Modify</button>
                      </li>
                    </ul>
               </div>
               :<div></div>
              
              }
                {this.state.modifyclick
                        ?<form onSubmit={(event) => {
                                event.preventDefault()
                                const inputContentChange = this.articleNewContent.value
                                this.updateContent(inputContentChange)
                              }}>
                              <div className="form-group mr-sm-2">
                                <div>Author:{this.props.resultAuthor}</div>
                                <div>ID of Article :{this.props.resultArticleId}</div>
                                <textarea
                                  id="articleNewContent"
                                  type="text"
                                  ref={(input) => { this.articleNewContent = input }}
                                  className="form-control"
                                  placeholder={this.props.resultSearch}
                                  required />
                              </div>
                              <button type="submit" class="btn" style={{backgroundColor:'#76AC00'}}>Update</button>
                        </form>
                        :<div></div>
                }
            </div>
          </main>
        </div>
      </div>
    );
  }
}
export default Main;