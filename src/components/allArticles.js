import React, { Component } from 'react';
//import Identicon from 'identicon.js';
import 'react-notifications-component/dist/theme.css'
import Modify from './Modify.js'
import { store } from 'react-notifications-component';


class allArticles extends Component {

  constructor(props){
    super(props);
    this.state={ModifyArticle:false , recive:false} 
    this.handleModifyInSowAll=this.handleModifyInSowAll.bind(this)
  }
/**
 * handle modify on click on modify
 */
  handleModifyInSowAll(){
    this.setState({
      ModifyArticle:true }); 
    
  }
  /**
   * show all articles that we already created by mapping
   */
  render() {
    return (
      <div className="container-fluid mt-5" style={{backgroundColor:'#F5F5F5'}}>
        <div className="row">
          <main role="main" className="col-lg-12 ml-auto mr-auto" style={{ maxWidth: '500px' }}>
            <div className="content mr-auto ml-auto">
            { this.props.articles.map((article, key) => {
                return(
                  <div className="card mb-4" key={key} >
                    <div className="card-header">
                      <small className="text-muted">{article.author}</small>
                    </div>
                    <ul id="articleList" className="list-group list-group-flush">
                      <li className="list-group-item">
                        <p><span>ID:{article.id}</span><span>***</span>{article.content}</p>
                        <p>{this.props.resultSearch}</p>
                        <button type="submit"  class="btn" style={{backgroundColor:'#76AC00'}} onClick={this.handleModifyInSowAll}>Modify</button>
                      </li>
                    </ul>
                  </div>
                )
              })}
            </div>
          </main>
        </div>
      </div>
    );
  }
}
export default allArticles;