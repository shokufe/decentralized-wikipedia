import React, { Component } from 'react';
//import Identicon from 'identicon.js';
import 'react-notifications-component/dist/theme.css'
import Modify from './Modify.js'
import { store } from 'react-notifications-component';


class History extends Component {

  constructor(props){
    super(props);
    this.state={ModifyArticle:false , recive:false} 
    
  }

  render() {
    return (
      <div className="container-fluid mt-5" style={{backgroundColor:'#F5F5F5'}}>
        <div className="row">
          <main role="main" className="col-lg-12 ml-auto mr-auto" style={{ maxWidth: '500px' }}>
            <div className="content mr-auto ml-auto">
                 <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">articleId</th>
                            <th scope="col">User</th>
                            <th scope="col">Old Content</th>
                            <th scope="col">New Content</th>
                            <th scope="col">Change Date</th>
                        </tr>
                    </thead>
                    { this.props.histories.map((history, key) => {
                        return(
                            <tr key={key} >
                                <th scope="row">{history.id}</th>
                                <td>{history.idArticle}</td>
                                <td>{history.author}</td>
                                <td>{history.oldcontent}</td>
                                <td>{history.newcontent}</td>
                                <td>{history.date}</td>
                            </tr>
                        )
              })}
                 </table>
            </div>
          </main>
        </div>
      </div>
    );
  }
}
export default History;